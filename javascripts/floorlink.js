
//floorlink support
function gotofloor(id,fl){
  var comment=$('#comment-'+id),f=comment.siblings('.floor-'+fl);
  if(f.size() > 0){
    $.scrollTo(f, 1000)
  }
}

var createFloorLink=function (id, fi){
  $('#fl-'+id+'-'+fi).click(function(e){
    var a = $(this).attr('id').split('-');
    gotofloor(parseInt(a[1]),parseInt(a[2]));
    e.preventDefault();
  }).poshytip({
    className: 'tip-twitter',
    alignX: 'center',
    showTimeout: 1,
    allowTipHover: false,
    fade: false,
    slide: false,
    //	hideTimeout: 50000,
    content: function(){
      var htmlc = $('#comment-'+id).parents('div').contents('.floor-'+fi).html() ;
      return htmlc;
    }
  })
}

function floorLink(){
  $('.comment-block').each(function(i,e){
    e = $(e);
    lz = $(this.parentNode.parentNode.parentNode).find('.author>a');
    lx = e.find(".userlogin>a")
    if (lz.length>0 && lx.length>0)
      if(lx[0].href==lz[0].href){
        e.find(".userlogin").attr('class','userlogin hostname')
      }
      
    var b = e.find('.body');
    var content = b.html();
    var id = parseInt(e.attr('id').replace('comment-',''));
    content=content.replace(/(\d+)(f|F|L|l|楼)/g, function(o,i){
      if($.browser.msie){
        setTimeout("createFloorLink("+id+", "+i+")", 200);
      }else{
        setTimeout(createFloorLink, 200, id, i);
      }
      return "<a href='#comment-"+id+"' id='fl-"+id+"-"+i+"'>"+o+"</a>";
    });

    b.html(content);
  });
}