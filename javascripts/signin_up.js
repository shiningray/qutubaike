// register verify functions
/*
function signup_validation(pos,msg_pos,url,key, msg_success,msg_failed,msg_empty)
{
    var _error = "<span class='verifi-failed'></span>";
    var _succe = "<span class='verifi-success'></span>";
    $(pos).change( function(){
        if(  (this).val().length == 0){
                if( msg_empty ) $(msg_pos).html(_error + msg_empty);
        }else{
                var obj = {};
                obj[key] = $(this).val();
                $.get(url, obj, function(text){
                  if(text == '1') $(msg_pos).html(_error + msg_failed);
                  else $(msg_pos).html( _succe + msg_success);
                });     
        }
    });
}
signup_validation('#user_login','#insertlogin', '/new4/signup_login', 'user_login', '', '已经使用','不能为空');
*/


$(function(){
    $('#user_login').change(function(){
           if( $(this).val().length == 0 ){
                $('#insertlogin').html("<span class='verifi-failed'></span>不能为空");
                return;
           }
           {$.get("/users/check_login", {user_login:$(this).val() },
           function(text){
                if( text=='1')$('#insertlogin').html("<span class='verifi-failed'></span>此用户名已被注册");
                else
                        $('#insertlogin').html("<span class='verifi-success'></span>");
                });
           }

       });

    $('#password').change( function(){
        var length = $(this).val().length;
        if(length == 0 ) $('#insertp1').html("<span class='verifi-failed'></span>密码不一致");
        else $('#insertp1').html("<span class='verifi-success'></span>");
    } );
    $('#user_password_confirmation').change( function(){
        var passcode1 = $(this).val();
        var passcode2 = $('#password').val();
        if( passcode1 == passcode2 ) $('#insertp2').html("<span class='verifi-success'></span>");
        else $('#insertp2').html("<span class='verifi-failed'></span>两次密码不同");
    } );
});


$(function(){
    $('#user_email').change(function(){
    var email=$('#user_email').val();
        if( $('#user_email').val()  == 0 ) { $('#insertemail').html("<span class='verifi-failed'></span>不能为空");return}
       if(isEmail(email)){$.get("/users/check_email",{user_email:$(this).val()},
           function(text){
             /*  if (text!="can_use"){ */
              if( text=='1')$('#insertemail').html("<span class='verifi-failed'></span>此邮箱地址已被使用");
                else
                        $('#insertemail').html("<span class='verifi-success'></span>");
       });
         }
       else
           {$('#insertemail').html("<span class='verifi-failed'></span>邮箱格式不正确");
           }

       })})
    function isEmail(str){
       //var reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/;
       var reg = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
       return reg.test(str);
}
var code = "";
$(function(){$('#invitation_code').live("focus",function(){
        var s = setInterval(invitationchange,500);
       })}
)


function invitationchange(){
        inputcode = $('#invitation_code').val().toUpperCase().replace(/ /g,"");
        if( inputcode.length!=16 ){
                $(':submit').attr('disabled','true');
                $('#reginfo').hide();
                $('#insertinvitation_code').html("<span class='verifi-failed'></span>邀请码为16位英文字母");
                return;
        }
        if(code==inputcode)return;
        $('#insertinvitation_code').html("校验邀请码中...");
            $.get("/new4/signup_code",{invitation_code:$('#invitation_code').val().toUpperCase().replace(/ /g,"")},
           function(text){
                code = $('#invitation_code').val().toUpperCase().replace(/ /g,"");
                if( text=='-1')
                {
                        $('#insertinvitation_code').html("<span class='verifi-failed'></span>此邀请码已被使用");
                        $(':submit').attr('disabled','true');
                        $('#reginfo').hide();
                }
                else if( text=='1')
                {
                        $('#insertinvitation_code').html("<span class='verifi-success'></span>");
                        $('#reginfo').show();
                        $(':submit').prop("disabled",false);
                }
                else
                {
                        $('#insertinvitation_code').html("<span class='verifi-failed'></span>邀请码无效");
                        $(':submit').attr('disabled','true');
                        $('#reginfo').hide();
                }
                });
}

$(function(){
	$('#new_user').submit( function(){
		return $('span.verifi-failed').length == 0 ;
	} );
})
