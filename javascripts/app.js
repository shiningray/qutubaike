function buffer(fun, ms, context){
   var buffer;
   return function(){
    if(buffer) return;
  buffer = setTimeout(function(){
     fun.call(this)
     buffer = undefined;
  }, ms);
   };
}

(function(){
  var oDiv=document.getElementById("float");
  if ( oDiv == undefined ) return false;
  var H=0,iE6;
  var Y=oDiv;
  while(Y){H+=Y.offsetTop;Y=Y.offsetParent};
  iE6=window.ActiveXObject&&!window.XMLHttpRequest;
  function myscroll(){
    var s=document.body.scrollTop||document.documentElement.scrollTop;
    if (s >H ){
      oDiv.className = "div1 div2";
      if(iE6){oDiv.style.top=(s-H)+"px";}
    }
    else
      oDiv.className = "div1";  
  }
  if(!iE6 || true){
    window.onscroll= buffer(myscroll,150,this );
  }
  

})();


$(function(){
    $('a.picture').fancybox();
})


$(function(){
  $('#tip-back').poshytip({
    className: 'tip-twitter',
    showTimeout: 1,
    alignTo: 'target',
    alignX: 'center',
    alignY: 'bottom',
    offsetY: 1,
    allowTipHover: false,
    fade: false,
    slide: false
  });
  $('#tip-feedback').poshytip({
    className: 'tip-twitter',
    showTimeout: 1,
    alignTo: 'target',
    alignX: 'center',
    alignY: 'bottom',
    offsetY: 1,
    allowTipHover: false,
    fade: false,
    slide: false
  });
  $('#tip-sina').poshytip({
    className: 'tip-twitter',
    showTimeout: 1,
    alignTo: 'target',
    alignX: 'center',
    alignY: 'bottom',
    offsetY: 1,
    allowTipHover: false,
    fade: false,
    slide: false
  });
  $('#tip-tencent').poshytip({
    className: 'tip-twitter',
    showTimeout: 1,
    alignTo: 'target',
    alignX: 'center',
    alignY: 'bottom',
    offsetY: 1,
    allowTipHover: false,
    fade: false,
    slide: false
  });
  $('#tip-message').poshytip({
    className: 'tip-twitter',
    showTimeout: 1,
    alignTo: 'target',
    alignX: 'center',
    alignY: 'bottom',
    allowTipHover: false,
    fade: false,
    slide: false
  });
  $('#tip-code').poshytip({
    className: 'tip-twitter',
    showTimeout: 1,
    alignTo: 'target',
    alignX: 'center',
    alignY: 'bottom',
    allowTipHover: false,
    fade: false,
    slide: false
  });
});