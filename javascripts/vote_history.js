var VoteHistory = {
	_Historys : null,
	_IsValid  : null,
	// set/get state for article
	// return 1,0,-1 for state up, normal, down
	voteState : function (article_id,state){
	
		if (!this.isValid()) return 0;
		if(state == null){
			return this._Historys[article_id];
		}else{
			this._Historys[article_id] = state;	//TODO: state 0(currently not available)
			this.saveHistory();
		}
	},

	isValid : function(){
		if(this._IsValid == null)
			this._IsValid = window.localStorage ? true : false;
		return this._IsValid;
	},

	// only read once
	readHistory : function(){
		this._Historys = window.localStorage.getItem('vote_history')
		this._Historys = JSON.parse(this._Historys)
		if(this._Historys == null) this._Historys ={}
	},

	saveHistory : function(){
		if ( this._Historys == null) return
		var maxItems = 500;
		var getKeys = function(obj){
		   var keys = [];
		   for(var key in obj){
		      keys.push(key);
		   }
		   return keys;
		}
		var keys = getKeys(this._Historys);
		if(keys.length > maxItems){
			var removes = keys.length - maxItems;
			for(i =0;i<removes;i++)
				delete this._Historys[keys[i]];
		}
		var data = JSON.stringify(this._Historys);
		window.localStorage.setItem('vote_history',data);
	},

	_init : function(){
		if(this.isValid() ) this.readHistory();
	},

	updateArticleStates : function(){
		if(! this.isValid() ) return;
		articles = [];
		$.each($('div[id^=qiushi_counts_]'),function(){articles.push(+this.id.replace('qiushi_counts_',''))});
		$.each(articles, function(i){
			var id = articles[i];
			var v = VoteHistory.voteState(id);
			if (v != undefined ){
				  var posscore = parseInt($('#up-'+id).text()),
			          negscore = parseInt($('#dn-'+id).text()),
			          d = (v>0?'up':'dn');
			      v>0 ? posscore++ : negscore--;
			      hidevotelink(id, d, posscore, negscore);
			}

		});
	}
};

VoteHistory._init();