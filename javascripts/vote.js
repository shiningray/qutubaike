function hidevotelink(id, d, p, n){
  var posscore,negscore;
  if(typeof p === 'undefined'){
    posscore = parseInt($('#up-'+id).text());
  }else{
    posscore = p;
  }
  if(typeof n === 'undefined'){
    negscore = parseInt($('#dn-'+id).text());
  }else{
    negscore = n;
  }

  $('#up-'+id).html(posscore) ;
  $('#dn-'+id).html(negscore) ;
  if( d == 'up')
    $('#vote-up-'+id).find('a').addClass('voted');
  else
    $('#vote-dn-'+id).find('a').addClass('voted');
  $('#vote-up-'+id).find('a').addClass('disable');
  $('#vote-dn-'+id).find('a').addClass('disable');
}

var voteQueue=[];
function vote2(id, v){
  if($('#up-'+id).hasClass('disable') || $('#dn-'+id).hasClass('disable') )  return;
  if(currentUser||currentGroup.options.allow_anonymous_vote){
    var posscore = parseInt($('#up-'+id).text()),
    negscore = parseInt($('#dn-'+id).text()),
    d = (v>0?'up':'dn');
    showAnimation('vote-'+d+'-'+id, v);
    $.getJSON('/articles/' + id + '/' + d);
    v>0 ? posscore++ : negscore--;
    hidevotelink(id, d,posscore, negscore);
    VoteHistory.voteState(id,v);
  }else{
    //voteQueue.push(v>0?id:-id);
    showLoginForm();
    $(document).bind('after_logged_in', function(){
      vote2(id, v);
    })
  }
}

function getCurUsr( data )
{
  if(data && data.user)
    currentUser=data.user;
  else
    currentUser = null;
  vote2m( curId, curV );
}
function mkvotestr( aid, uid , v )
{
  return v>0?_Base64.encode(aid+'+'+uid) : _Base64.encode(aid+'-'+uid);
}

var	curId;
var	curV;
function vote2m(id, v){
  if( !currentUser )
  {
    if( $.readCookie('auth_token') )
    {
      curId = id;
      curV = v;
      $.getJSON('/session.js?'+(new Date().getTime()), getCurUsr);
    }
    else
      window.location.href = '/login';
  }
  else{
    var posscore = parseInt($('#pos-score-'+id).text()),
    negscore = parseInt($('#neg-score-'+id).text()),
    d = (v>0?'up':'dn');
    showAnimation(d+'-'+id, v);
    //  $.get('/articles/'+id+'/'+d);
    v>0 ? posscore++ : negscore--;
    hidevotelink(id, posscore, negscore);
    $.get( '/new3/vote/='+mkvotestr(id,currentUser.id,v) ) ;
  }

//voteQueue.push(v>0?id:-id);
}