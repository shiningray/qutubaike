/* after logged in, the document will receive 'after_logged_in' event */
function showLogin(){	
  if($.browser.msie){
    document.styleSheets[0].addRule('.login', 'display:block !important');
  }else{
    document.styleSheets[0].insertRule('.login{display:block !important;}',0);
  }
  showLogout('none');
  $(document).trigger('login_loaded');
}

function showSuggest(){
  if($.browser.msie){
    document.styleSheets[0].addRule('.suggest-form', 'display:inline !important');
  }else{
    document.styleSheets[0].insertRule('.suggest-form{display:inline !important;}',0);
  }
}

function showLogout(type){
  type = type || 'inherited';
  if($.browser.msie){
    document.styleSheets[0].addRule('.logout', 'display:'+type+' !important');
  }else{
    document.styleSheets[0].insertRule('.logout {display:'+type+' !important;}',0);
  }
}

function loadLoginCookie(){
  var user = $.readCookie('user');
  if(user){
    try{
      //user = '{"unread_messages_count": 0, "user": {"login": "'+_Base64.decode(user)+'"}}';
      user = eval('('+user+')');
      loadLogin(user);
      return user;
    }catch(e){
      alert(e);
    }
  }
  if($.readCookie('auth_token') || $.readCookie('_session_id'))
    return $.getJSON('/session.js?'+(new Date().getTime()), loadLogin);
  else
    return '{}'
}

function loadLogin(data){
  if(data){
    currentUser = data;
    $('.username').text(currentUser.login);
    //$('.login').toggleClass( "hidden" );
    showLogout('none');
    if(data.unread_messages_count > 0){
      $('#unread_messages_count').text(data.unread_messages_count);
      document.title = '您收到'+data.unread_messages_count+'个小纸条 | '+document.title;
      $('#unread_messages_count').css('display','');
    }
    else{
      $('#unread_messages_count').hide();
    }
    if(data.unread_notifications_count && data.unread_notifications_count > 0){
      $('#unread_notifications_count').text(data.unread_notifications_count);
    }
    showLogin();
    VoteHistory.updateArticleStates();
    queryNewMessage();
  } else {
    currentUser=null;
    showLogout();
  }
}
$(loadLoginCookie);

function showLoginForm(){
  $('#login-form').lightbox_me({
    centered: true,
    onLoad: function() {
      $('#login').focus()
    }
  });
}