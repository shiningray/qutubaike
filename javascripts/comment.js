function article_comments_path(id){
  return '/articles/'+id+'/comments';
}

function loadComments(e){
  var l=$(this);
  var id = /\d+/.exec(l.attr('id'));
  if(!id) return;
  id=id[0];
  var comments_el = $('#qiushi_comments_'+id);
  if(comments_el.size() == 0){
    var xx = l.html();
    l.text('...');
    $.get(article_comments_path(id), null, function(data){
      $('#qiushi_counts_'+id).after(data).toggleClass('qiushi_counts_afterclick');
      $("#l"+id).height( $('#r'+id).height() ) ;
      comments_el.show();
      l.html(xx).trigger('loaded');
      // l.html(xx);
      if(typeof decrypt == 'function'){
        l.ready(decrypt)
      }
      if(currentUser){
        l.ready(function(){
          $('.comments .current_user-name').text(currentUser.login);
        })        
      }
      l.ready(floorLink);
    });
  }else{
    comments_el.toggle();
    $('#qiushi_counts_'+id).toggleClass('qiushi_counts_afterclick');
  }
  //  window.location.hash = l.attr('id');
  l.blur();
  e.preventDefault();
}

function showall(id){
  $('.hide', '#qiushi_comments_'+id).toggle();
}

function closeComments(e){
  var l = $(this) ;
  var id = /\d+/.exec( l.attr('id' )) ;
  id = id[0];
  $('#qiushi_comments_'+id).toggle() ;
  window.location.hash = '#qiushi_counts_'+id;
}

$(document).keypress(function(e){
  if(e.ctrlKey && e.which == 13 || e.which == 10) {
    var o=e.target;
    if(o.form){
      var f=$(o.form).find('input.comment_submit');
      postComment.call(f[0]);
    }
  }
});

$(function(){
  $(document).bind("login_loaded", function(){
    $('.comments .current_user-name').text(currentUser.login);
  });
})