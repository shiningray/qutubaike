// vote-records
// application
// app.js
// report
// ajaxlogin

var l = window.location.href;
var currentUser;
//$(document).ready(function(){ VoteHistory._init(); VoteHistory.updateArticleStates();});

// application.js
function shareToSina(id){
  var s=screen,d=document,e=encodeURIComponent,a=$('#article'+id),u=$('.permlink', '#qiushi_counts_'+id).attr('href');
  var f='http://v.t.sina.com.cn/share/share.php?',p=['url=',e(u),'&title=',e('糗事'+id),'&appkey=2924220432'].join('');
  var a=function(){
    if(!window.open(f+p,'mb',['toolbar=0,status=0,resizable=1,width=620,height=450,left=',(s.width-620)/2,',top=',(s.height-450)/2].join('')))u=f+p;
  };
  if(/Firefox/.test(navigator.userAgent)){setTimeout(a,0)}else{a()}
}

function showAnimation(containerId, actionValue){
    var obj = $('#'+containerId),
        pos = obj.offset(),
        ani = $('<div id="vote-ani" class="'+(actionValue > 0 ? "pos" : "neg")+'" style="font-size:10px;z-index:1000">'+(actionValue > 0 ? "+1" : "-1")+"</div>");
    ani.appendTo('body');
//    pos.top += $(document).scrollTop()+5;
//    pos.left += $(document).scrollLeft()+30;
	  pos.top += 7;
	  pos.left += 30;
	  if(actionValue < 0 )
	     pos.left += 5;
    ani.offset(pos).css('display', 'block').animate({'font-size': '64px', opacity: 0, left: "-=40px"}, 350, 'linear', function(){ani.remove()});
}

function SelfXY(){
    var yScrolltop;
    var xScrollleft;
    if (self.pageYOffset || self.pageXOffset) {
        yScrolltop = self.pageYOffset;
        xScrollleft = self.pageXOffset;
    } else if (document.documentElement && document.documentElement.scrollTop || document.documentElement.scrollLeft ){     // Explorer 6 Strict
        yScrolltop = document.documentElement.scrollTop;
        xScrollleft = document.documentElement.scrollLeft;
    } else if (document.body) {// all other Explorers
        yScrolltop = document.body.scrollTop;
        xScrollleft = document.body.scrollLeft;
    }
    arrayPageScroll = new Array(xScrollleft + event.clientX ,yScrolltop + event.clientY)
    return arrayPageScroll;
}


/**
 *  速度标记
 *
 */

function SpeedTester(){
    this.displayControlId = "divSpeed";

    this.goodColor = "#33CC33";
    this.averageColor = "#3300FF";
    this.badColor = "#FF0033";

    this.goodMessage = "仅用{0}秒就载入了整个页面，您的网络真棒";
    this.averageMessage = "载入整个页面用了{0}秒，您的网络马马虎虎";
    this.badMessage = "居然用了{0}秒才把整个页面载入进来，实在是抱歉";
    this.goodSpeed = 10000;
    this.averageSpeed = 50000;
    this.badSpeed = 100000;

    this.beginTest = function()    {
        this.startTime = new Date();

        window.onload = function(){
            var displayControl =document.getElementById(SpeedTester.displayControlId);
            if(!displayControl)return;
            SpeedTester.endTime = new Date();

            var spentTime = SpeedTester.endTime - SpeedTester.startTime;

            if (spentTime <= SpeedTester.goodSpeed){
                displayControl.style.backgroundColor = SpeedTester.goodColor;
                displayControl.title = SpeedTester.goodMessage.replace("{0}", spentTime / 1000);
            }else if (spentTime <= SpeedTester.averageSpeed){
                displayControl.style.backgroundColor = SpeedTester.averageColor;
                displayControl.title = SpeedTester.averageMessage.replace ("{0}", spentTime / 1000);
            }else{
                displayControl.style.backgroundColor = SpeedTester.badColor;
                displayControl.title = SpeedTester.badMessage.replace("{0}", spentTime / 1000);
            }
        }
    }
}

SpeedTester = new SpeedTester();
SpeedTester.beginTest();

function addBookmark(title,url) {
    if (window.sidebar) {
        window.sidebar.addPanel(title, url,"");
    } else if( document.all ) {
        window.external.AddFavorite( url, title);
    } else if( window.opera && window.print ) {
        return true;
    }
    return false;
}

function watch(id){
  $('#favorite-'+id).children('a').attr('href', '/articles/remove_favorite/'+id).html('取消围观').removeClass('star').addClass('stared');
}

function unwatch(id){
  $('#favorite-'+id).children('a').attr('href', '/articles/add_favorite/'+id).html('围观').removeClass('stared').addClass('star');
}

$(function(){
    $('.favorite-button').click(function(){
      if ( $(this).children('a').attr('href')!="/login" ){
        new $(this).load($(this).children('a').attr('href'));
        return false;}
    else
        return true;
    });
    $('input.numeric').keydown(function(e){
        var k = e.keyCode;
        if(((k>47)&&(k<58)) ||
            (k == 8) ||
            (k == 46)||
            (k == 13)||
            (k>=96 && k<=105)){
            //            event.returnValue = true;
            return true;
        } else {
            e.returnValue = false;
            return false;
        }
    });
});

function open_form(id){
    $('#quickform-' + id).show();
    $('#reply-'+id).hide();
}

function close_form(id){
    $('#quickform-' + id).hide();
    $('#reply-'+id).show();
}

function reply(id){
    var f = $('#quickform-' + id);
    $.ajax({
        type: "POST",
        url: f.attr('action'),
        data: f.serialize(),
        success: function(){
            $('#comment-'+id)
        }
    })
    close_form(id);
    return false;
}



var COMMENT_WARNING=    '请不要发表与本内容无关的评论，您有了账号就是有身份的人了，我们可认识您。';
function clear_warning(e){
    var t = $(this);
    t.focus();
    if($.trim(t.val()) == COMMENT_WARNING){
        t.val('');
	t.removeClass('original');
    }
    t.blur(function(){
        var t=$(this);
        if($.trim(t.val())==''){
            t.val(COMMENT_WARNING);
            t.addClass('original');
        }
    });
}

function postComment(){
    var e =$(this),f = this.form, fe = $(f);
    var v = $.trim(fe.find('.comment_input').val());
    if(v == '' || v == COMMENT_WARNING){
        return false;
    }

    $.post(f.action, fe.serialize(), function(data){
        e.val('发表评论').attr('disabled', false);
        fe.find(".comment_input").val('').height('50px');
        var u = $('#r'+fe.attr('data-article_id'));
        if(u.size()>0){
			u.append(data);
		}
        else{
          $(data).insertBefore(fe);
        }
    });
    this.value = ('正在发表');
    this.disabled = true;

    return false;
}

function showall(id){
  $('.hide', '#qiushi_comments_'+id).toggle();
}
$(function(){
    $('#comment_submit').live('click', postComment);
    $('.comment_input').live('click', clear_warning).live('mouseover', clear_warning);
    var hash=window.location.hash;
    if(hash.indexOf('#c-') === 0){
        $(hash).click();
    }
});



function loadScores(){
    var ids=[];
    $('.article').each(function(i,e){
        var id = parseInt($(e).attr('id').replace('article', ''));
        if(!isNaN(id) && id > 0){
            ids.push(id);
        }
    })
    if(ids.length == 0)return;
    $.getJSON('/scores', {ids:ids.join(' ')}, function(data, status){
        $.each(data, function(id, value){
            var s = value.score;
            $('#pos-score-'+id).text(s.pos);
            $('#neg-score-'+id).text(s.neg);
            if(value.rated){
                hidevotelink(id,s.pos,s.neg);
            }
            if(typeof value.watched != 'undefined'){
              if(value.watched){watch(id);}else{unwatch(id);}
            }
            if(s.public_comments_count==0){
                $('#c-'+id).text('暂无评论');
            }else{
                $('#c-'+id).text(s.public_comments_count+'条评论');
            }
        });
    })
}
$(loadScores);

/*
$(function(){
$('.comment').live('mouseenter', function(){
  $('.comment-score a', this).attr('visibility', 'display');
}).live('mouseleave', function(){
  $('.comment-score a', this).attr('visibility', 'hidden');
})
})*/
function comment_vote(id,s){
  var se = $('#comment-'+id+' .score');
  se.text(parseInt(se.text())+s);
  $('#comment-score-'+id+' a').css('visibility', 'hidden');
  if('jStore' in jQuery){
    if(jQuery.jStore.CurrentEngine.get('c'+id)){return}
    jQuery.jStore.CurrentEngine.set('c'+id, true);
  }
  $.post('/comments/'+id+(s > 0 ? '/up' : '/dn'));
  //return false;
}

$(function(){
  $('.comment').live('mouseenter', function(){
    $(this).addClass('hover');
    if(currentUser){
      $(this).children('.reply').css('visibility','visible');
      $(this).children('.report').css('display','inline');
    }
  }).live('mouseleave', function(){
    $(this).removeClass('hover');
    $(this).children('.reply').css('visibility', 'hidden')
    $(this).children('.report').css('display','none');
  })
})
function replyComment(comment_id, article_id, floor){
  var form = $('form', '#qiushi_comments_'+article_id), c = $('#comment-'+comment_id);
  $('input[name=comment[parent_id]]',form).val(comment_id);

  var t = $('textarea', form),o = t.val();
  nv = '回复'+floor+'L:'+ (o == COMMENT_WARNING ? '' : o);
  t.val(nv);
  $.scrollTo(form, 1000);
  t.focus();
  t.setCursorPosition(nv.length);
}


//$(floorLink);

//app.js

function dbclose(e)
{
	$(this).toggle();
	var l = $(this) ;
	var id = /\d+/.exec( l.attr('id' )) ;
	id = id[0];
	window.location.hash = '#qiushi_counts_'+id;
}

var	qiushiMap = {};
var	qiushiOff = 2;
var	theme = 'hot' ;
var	bMore = true;

function tagRepl()
{
	var	ret = 0 ;
	$(".untagged").each( function(i){

		$(this).toggleClass('untagged') ;
		if( qiushiMap[$(this).attr("id")] )
			$(this).hide();
		else {
			qiushiMap[$(this).attr("id")] = 1;
			ret ++ ;
		}

	});
}

function showMore()
{
	$("#loadingBar").show();
	$("#loadError").hide();


	$.ajax( {
		type :"GET" ,
		url :  '/new2/'+theme+'/5/page/'+ qiushiOff++ +'?more',
		dataType : "html",

		success:function( data ) {
			$(".qiushiData").append(data);
			$("#loadingBar").hide();
			if( tagRepl() <= 0 )
				$("#loadError").show();
		},
		 error:function (xhr, ajaxOptions, thrownError){
			qiushiOff -- ;
			$("#loadingBar").hide();
			$("#loadError").show();
                }
	});

}
function truncTitle( text )
{
	var i = 0 ;
	var ret = 0;
	var str = text.replace( /[ \-=\n]+/g, ' ' );
	return  str.substring(0,20) ;

}
function shareQiushi()
{
	var l = $(this) ;
	var id = /\d+/.exec( l.attr('id' )) ;
	id = id[0];
	var node = $('#qiushi_tag_'+id+' div.content' ) ;
	var pic = $('#qiushi_tag_'+id+' div.thumb img' ) ;
	var title = truncTitle( node.text() ) ;

	var msg = {msg:title, title:"糗事百科", pic: pic?"":pic.attr("src"),
		desc:node.text(), param:"" };
	qplus.system.shareApp( msg );

	alert( title ) ;
}
$(function(){
    $('a.qiushi_comments').live( "click", loadComments);
	$('a.more').click( showMore ) ;
    $('a.closebtn').live( "click", closeComments) ;
    $('div.comments-close').live( "click", closeComments) ;
    $('a.sharebtn').live( "click", shareQiushi) ;
    $('div#loadError').live( "click", showMore) ;
    $("div.block").hover(function(){
	$(this).find("li.uptip span").fadeIn(300);}, function(){$(this).find( "li.uptip span" ).fadeOut(300);});
    // $('div.comments').live("dblclick",  dbclose );
//	$('a#logintop').click( showLoginForm ) ;
});

$(document).ready(function (){
	$("a").bind("focus",function() {
		if(this.blur) {this.blur()};
	});
	//$("#loadingBar").hide();
	//$("#loadError").hide();
	var regex = /\/new2\/(\w+)/.exec( document.URL ) ;
	if( regex )
		theme = regex[1];
	/*
    $(".col2").scroll(function(){
	  if( qiushiOff > 95 )
		bMore = false ;
          if( bMore && $(this)[0].scrollTop + $(this).height() == $(this)[0].scrollHeight )
		showMore();
	 });
	*/
});

//report
var showReport;
$(function(){
  showReport = function(comment_id){
    var el = $('#comment-'+comment_id+' .report'), o = el.offset(), cmt=$('#comment-'+comment_id);
    o.top += el.height();
    o.left += el.width();
    $('#report-form').trigger('close');
    cmt.addClass('highlight');
    $('#report-form input[name=comment_id]').val(comment_id);
    $('#report-form').css('display', 'block').offset(o);
  }
  $('#report-form').bind('close', function(){
    $(this).css('display', 'none');
    $('.highlight').removeClass('highlight');
  });
  $('#report-form form').submit(function(){
    var cmt_id = this.comment_id.value, a = this.action = 'http://n17.qiushibaike.com/comments/'+ cmt_id + '/report',
        f = $(this);
    submit_button=$('button[type=submit]', this).attr('disabled', 'disabled');
    $.post(a, f.serialize(), function(){
     // submit_button.attr('disabled', '');
	submit_button.removeAttr( 'disabled' ) ;
      $('#report-form').trigger('close');
	$('#report-form').find('input[type=radio]').prop('checked',false);
    });
   submit_button.removeAttr( 'disabled' ) ;
   $('#report-form').trigger('close');
   $('#report-form').find('input[type=radio]').prop('checked',false);
    return false;
  });
  $('#close-form').click(function(event){
    event.preventDefault();
    $('#report-form').trigger('close');
  });
});


//ajax login
function showSuggestForm(){
  $('#suggest-form').lightbox_me({
  centered: true,
  onLoad: function() {
      $('#suggest-text').focus()
      }
  });
}
function startQueryNewMessage()
{
	$(document).ready(function() {
		setInterval('queryNewMessage()',5*60*1000);
	});
}
var msg_regexp = new RegExp('您收到\\d+个小纸条');

function queryNewMessage(){
	if(currentUser){	
        	$.ajax({
                    type: "GET",
                    url: "/messages/count?query_new_msg=" + Math.floor(Math.random()*316661),
                    dataType: "text",
                    success: function(msg) {
			newmsg = parseInt(msg);
                    	if(newmsg && newmsg>0){
				$('#unread_messages_count').text(newmsg);
				reg_result = msg_regexp.exec(document.title)
				if(reg_result){
					document.title = document.title.replace(reg_result,'您收到'+newmsg+'个小纸条')
				}
				else{
					document.title = '您收到'+newmsg+'个小纸条 | '+document.title;
				}
				$('#unread_messages_count').css('display','');
			}
                    }
        	});
	}
}
$(startQueryNewMessage);
$(function(){
  $('.need-login').click(function(e){
    e.preventDefault();
    showLoginForm();
  });
  $('#login-form button[type=submit]').click(function(e){
    e.preventDefault();
    var form = $(this);
    var name = $(this.form).find('input#login').val();
    var pass = $(this.form).find('input#password').val();
    if(name.length ==0 || pass.length ==0) return false;
    this.disabled=true;
    this.value='正在登录...';
    $(this.form).submit();
    return false
  });

  $('#suggest-form button[type=submit]').click(function(e){
    e.preventDefault();
    this.disabled=true;
    this.value='正在提交...';
    $(this.form).submit();
    return false
  });

  $('#suggest-form form').submit(function(e){
    e.preventDefault();
    var form = $(this);
    $.post('/new2/suggest', form.serialize(), function(data){

      $('#suggest-form').trigger('close');
      $('button[type=submit]', form).attr('disabled', '').val('提交建议');
      $('#suggest-text')[0].value = '';
  	$('#suggest-form button[type=submit]').attr("disabled", false ) ;

    }, 'json')
    return false;
  })
  $('#login-form form').submit(function(e){
    e.preventDefault();
    var form = $(this);
    $.post('/session.js', form.serialize(), function(data){
      $('button[type=submit]', form).attr('disabled', '').val('登录');

    	$('#login-form button[type=submit]').attr("disabled",false);
      if('error' in data){
        form.children('#error').text(data['error']);
	form.find('.inputbox input').addClass('error');
	$('#login-form').effect("shake",{times:3,distance:10},100);
      }else{
        /*
        if($.browser.msie){
          document.styleSheets[0].removeRule('.logout');
        }else{
          if(document.styleSheets[0].cssRules[0].selectorText == '.logout'){
            document.styleSheets[0].deleteRule(0);
          }
        }*/
        loadLogin(data);
        $('#login-form').trigger('close');
        $(document).trigger('after_logged_in');
      }
    }, 'json')
    return false;
  })
});